//package gpb.account;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
//
//import java.util.HashMap;
//import java.util.Map;
//
////@SpringBootTest
//@AutoConfigureMockMvc
//class GpbAccountApplicationTests{
//
//	@Autowired
//	private  MockMvc mockMvc;
//
//	private final ObjectMapper objectMapper = new ObjectMapper();
//
//	@Test
//	void contextLoads() throws Exception {
//
//		Map<String, String> map = new HashMap<>();
//
//		map.put("clientId","1040");
//		map.put("clientType","RETAIL");
//		map.put("accountType", "DEPOSIT");
//
//		mockMvc.perform(MockMvcRequestBuilders
//				.post("/account")
//				.accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsString(map))
//		).andExpect(MockMvcResultMatchers.status().isOk());
//	}
//
//}
